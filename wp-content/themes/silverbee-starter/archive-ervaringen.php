<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Silverbee_Starter
 */

get_header(); ?>
    <article>
        <div class="container-fluid">
            <div class="header-page-wrapper">
                <div class="row justify-content-center">
                    <div class="col-11 col-xl-7">
                        <h1 class="fancy">
                            Ervaringen
                        </h1>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-11 col-lg-11 col-xl-7">
					<?php
					while ( have_posts() ) : the_post();
						get_template_part( 'template-parts/content', 'ervaringen' );
					endwhile; // End of the loop.
					?>
                </div>
            </div>
            <div class="portfolio-slider-wrapper">
                <div class="row justify-content-center">
                    <div class="col-12">
						<?php get_template_part( 'template-parts/portfolio', 'items' ); ?>
                    </div>
                </div>
            </div>
        </div>
    </article>
<?php
get_footer();

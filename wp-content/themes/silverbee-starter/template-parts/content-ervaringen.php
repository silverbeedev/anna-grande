<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Silverbee_Starter
 */

$pagina_id             = get_the_ID();
$pagina_inleiding      = get_field( 'pagina_inleiding', $pagina_id );

?>

<section class="entry-content ervaring-item">
	<div class="row justify-content-center">
		<div class="col-12 col-lg-12">
		</div>
		<div class="col-12">
			<h4>
				<?php echo get_the_title(); ?>
			</h4>
			<p class="mb-1">
				<em>
					<?php echo $pagina_inleiding; ?>
				</em>
			</p>
			<?php
			$content = get_the_content();
			$trimmed_content = wp_trim_words( $content, 50, NULL );
			echo $trimmed_content;
			?>
			<a href="<?php echo get_permalink(); ?>">Lees meer</a>
		</div>
	</div>
</section>




<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 24/12/2018
 * Time: 15:05
 */

// ARGUMENTS FOR workshop
$args = array(
	'posts_per_page' => - 1,
	'orderby'        => 'date',
	'order'          => 'DESC',
	'post_type'      => 'ervaringen',
	'post_status'    => 'publish',
);

$ervaringen = get_posts( $args );

?>


<div class="slider-wrapper">
    <div id="carousel-ervaringen" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
			<?php
			$j = 0;
			foreach ( $ervaringen as $ervaring ) : ?>
                <li data-target="#carousel-ervaringen"
                    data-slide-to="<?php echo $j; ?>" <?php echo ( $j == 0 ) ? 'active' : '' ?>></li>
				<?php $j ++; endforeach; ?>
        </ol>
        <div class="carousel-inner">
			<?php
			$i = 0;
			foreach ( $ervaringen as $ervaring ) : ?>
                <div class="carousel-item <?php echo ( $i == 0 ) ? 'active' : '' ?>"
                     style="background-image: url('<?php echo get_the_post_thumbnail_url( $ervaring ); ?>'); ">
                        <div class="item-content">
                            <div class="item-content-inner">
                                <h3>
									<?php echo $ervaring->post_title; ?>
                                </h3>
                                <p>
									<?php echo $ervaring->post_excerpt; ?>
                                </p>
                                <a href="<?php echo get_the_permalink( $ervaring ); ?>" class="btn-primary">
                                    Lees meer
                                </a>
                            </div>
                        </div>
                </div>
				<?php
				$i ++;
			endforeach; ?>
        </div>
        <a class="carousel-control-prev" href="#carousel-ervaringen" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carousel-ervaringen" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>


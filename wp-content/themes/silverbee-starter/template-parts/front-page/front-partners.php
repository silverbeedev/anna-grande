<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 24/12/2018
 * Time: 15:46
 */

$args = array(
	'posts_per_page' => - 1,
	'post_type'      => 'partners',
	'post_status'    => 'publish',
);

$partners = get_posts( $args );

?>

<div class="slider-wrapper">
    <h2 class="fancy">
        Partners
    </h2>
    <div id="carousel-partners" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
			<?php
			$i = 1;
			$control = false;
            $count = count($partners);
            if ( $count > 3 ) {
                $control = true;
            }

			foreach ( $partners as $partner ) :
			$partner_id = $partner->ID;

			?>
			<?php if ( $i == 1 ): ?>
            <div class="carousel-item <?php echo ( $i == 1 ) ? 'active' : '' ?>"><?php endif; ?>
                <div class="item-img item-<?php echo $i; ?>">
                    <img src="<?php echo get_the_post_thumbnail_url( $partner_id ); ?>"
                         alt="<?php echo get_the_title() ?>">
                </div>
				<?php if ( $i % 4 == 0 ): ?></div>
            <div class="carousel-item"><?php endif; ?>
				<?php
				$i ++;
				endforeach;
				?>
            </div>
        </div>
        <?php if ($control === true) : ?>
        <a class="carousel-control-prev" href="#carousel-partners" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carousel-partners" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
        <?php endif; ?>
    </div>
</div>


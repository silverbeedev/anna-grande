<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 24/12/2018
 * Time: 12:57
 */ ?>

<?php

// ARGUMENTS FOR INDIVIDUELE COACHING
$args_individueel = array(
	'posts_per_page' => - 1,
	'orderby'        => 'date',
	'order'          => 'DESC',
	'post_parent'    => 25,
	'post_type'      => 'page',
	'post_status'    => 'publish',
);

$front_individueel = get_posts( $args_individueel );

// ARGUMENTS FOR GROEPSCOACHING
$args_groep = array(
	'posts_per_page' => - 1,
	'orderby'        => 'date',
	'order'          => 'DESC',
	'post_parent'    => 27,
	'post_type'      => 'page',
	'post_status'    => 'publish',
);

$front_groepscoaching = get_posts( $args_groep );

// ARGUMENTS FOR workshop
$args_workshop = array(
	'posts_per_page' => -1,
	'orderby'        => 'date',
	'order'          => 'DESC',
	'post_type'      => 'workshops',
	'post_status'    => 'publish',
);

$front_workshops = get_posts( $args_workshop );
?>

<section class="entry-content front-content-cpt">
    <div class="child-page-wrapper">
        <div class="row">
            <div class="col-md-6 col-lg-6 col-xl-4 col-child">
                <div class="child-page-item child-page-item--front">
                    <div class="item-thumb">
                        <a href="<?php echo get_the_permalink(25); ?>">
                            <img src="<?php echo get_the_post_thumbnail_url( 25 ); ?>" alt=""/>
                        </a>
                        <h2>
							<?php echo get_the_title( 25 ); ?>
                        </h2>
                    </div>
                    <div class="item-excerpt">
                        <ul>
							<?php foreach ( $front_individueel as $individueel ) : ?>
                                <li>
                                    <span><?php echo $individueel->post_title; ?></span>
                                    <a href="<?php echo get_the_permalink($individueel->ID); ?>" class="btn-primary">
                                        Meer info
                                    </a>
                                </li>
							<?php endforeach; ?>
                        </ul>
                    </div>
                    <div class="item-cta">
                        <a href="<?php echo get_the_permalink(25 ); ?>">
                            Bekijk alle individuele coaching
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-6 col-xl-4 col-child">
                <div class="child-page-item">
                    <div class="item-thumb">
                        <a href="<?php echo get_the_permalink(27); ?>">
                            <img src="<?php echo get_the_post_thumbnail_url( 27 ); ?>" alt=""/>
                        </a>
                        <h2>
							<?php echo get_the_title( 27 ); ?>
                        </h2>
                    </div>
                    <div class="item-excerpt">
                        <ul>
							<?php foreach ( $front_groepscoaching as $groepcoaching ) : ?>
                                <li>
                                    <span><?php echo $groepcoaching->post_title; ?></span>
                                    <a href="<?php echo get_the_permalink($groepcoaching->ID); ?>" class="btn-primary">
                                        Meer info
                                    </a>
                                </li>
							<?php endforeach; ?>
                        </ul>
                    </div>
                    <div class="item-cta">
                        <a href="<?php echo get_the_permalink(27 ); ?>">
                            Bekijk alle groepscoaching
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-6 col-xl-4 col-child">
                <div class="child-page-item">
                    <div class="item-thumb">
                        <a href="<?php echo get_the_permalink(29); ?>">
                            <img src="<?php echo get_the_post_thumbnail_url( 29 ); ?>" alt=""/>
                        </a>
                        <h2>
					        <?php echo get_the_title( 29 ); ?>
                        </h2>
                    </div>
                    <div class="item-excerpt">
                        <ul>
					        <?php foreach ( $front_workshops as $workshop ) : ?>
                                <li>
							        <span><?php echo $workshop->post_title; ?></span>
                                    <a href="<?php echo get_the_permalink($workshop->ID); ?>" class="btn-primary">
                                        Meer info
                                    </a>
                                </li>
					        <?php endforeach; ?>
                        </ul>
                    </div>
                    <div class="item-cta">
                        <a href="<?php echo get_the_permalink(29 ); ?>">
                            Bekijk alle workshops
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php
$args = array(
    'posts_per_page' => -1,
	'orderby'        => 'date',
	'order'          => 'DESC',
	'post_type'      => array('workshops', 'page'),
	'meta_query'     => array(
        array(
          'key'     => '_uitgelicht_meta_key',
          'value'   => 'checkbox',
          'compare' => '='
        )
      ),
	'post_status'    => 'publish',
);

$workshops = get_posts( $args );

?>
	<?php foreach ( $workshops as $workshop ) :
		$workshop_id = $workshop->ID;
		?>

        <div class="header-workshop-item">
            <div class="row">
                <div class="col-12 col-md-5 col-lg-5">
                    <div class="workshop-left" style="background-image: url('<?php echo get_the_post_thumbnail_url($workshop_id) ?>')">

                    </div>
                </div>
                <div class="col-12 col-md-7 col-lg-7 align-self-center">
                    <div class="workshop-right">
                        <div class="workshop-title">
		                    <?php echo $workshop->post_title; ?>
                        </div>
                        <div class="workshop-content">
		                    <?php echo $workshop->post_excerpt; ?>
                        </div>
                        <div class="workshop-cta">
                            <a href="<?php echo get_the_permalink($workshop); ?>" class="btn-primary">
                                Meer info
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


	<?php endforeach; ?>


<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 22/01/2019
 * Time: 13:15
 */

?>

<div class="agenda-archive-wrapper">
	<?php
	$posts = get_posts( array(
		'post_type'      => 'agenda',
		'posts_per_page' => - 1,
		'meta_key'       => 'datums_0_datum',
		'orderby'        => 'meta_value',
		'order'          => 'ASC'
	) );
	if ( have_posts() ) : while ( have_posts() ) : the_post();
		$my_workshop_name    = get_the_title();
		$my_workshop_content = get_the_content();
		$my_workshop_item    = get_field( 'agenda_item' );
		$title               = get_the_title();
		$the_ID              = get_the_ID();

		if ( have_rows( 'datums' ) ):
			?>

            <div class="agenda-item">
                <div class="row">
					<?php
					if ( get_field( 'agenda_item' ) ):
						?>
                        <div class="agenda-type">
                            workshop
                        </div>
					<?php
					endif;
					?>
                    <div class="col-12 col-sm-2 align-self-center">
						<?php
						while ( have_rows( 'datums' ) ) : the_row();

							$my_workshop_date = get_sub_field( 'datum' );
							?>
                            <div class="agenda-date">
								<?php
								$originalDate = $my_workshop_date;
								$formDate     = date( 'd-M-Y', strtotime( $originalDate ) );
								$newDate      = date_i18n( '<\s\p\a\n\>d<\/\s\p\a\n\> M', strtotime( $originalDate ) );
								echo $newDate ?>
                            </div>
						<?php
						endwhile;
						?>
                    </div>
                    <div class="col-12 col-sm-10 align-self-center text-center text-sm-left">
                        <div class="agenda-item-title">
                            <h2 class="item-title">
								<?php echo $my_workshop_name ?>
                            </h2>
                        </div>
                        <div class="agenda-item-content">
							<?php echo $my_workshop_content ?>
                        </div>
                        <div class="agenda-inschrijven">
                            <a class="btn-primary" id="<?php echo $my_workshop_name; ?>"
                               data-toggle="modal" data-target="#inschrijfmodal-<?php echo get_the_ID(); ?>">
                                Inschrijven
                            </a>
                        </div>
                    </div>
                </div>
            </div>
		<?php
		endif;
	endwhile;
	endif;
	?>

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <div class="modal fade inschrijfmodal" id="inschrijfmodal-<?php echo get_the_ID(); ?>" tabindex="-1"
             role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Inschrijven voor: <span class="modal-info"></span>
                        </h5>
                    </div>
                    <div class="modal-body">
                        <form action="" method="post" class="inschrijf-form">
                            <div class="form-row">
                                <label for="date">Datum*</label>
                                <select name="date" id="date">
									<?php
									if ( have_rows( 'datums' ) ) :
										echo get_the_title();
										echo get_the_ID();

										while ( have_rows( 'datums' ) ) : the_row();
											$my_workshop_date = get_sub_field( 'datum' );
											$originalDate     = $my_workshop_date;
											$formDate         = date_i18n( 'd M Y', strtotime( $originalDate ) ); ?>
                                            <option value="<?php echo $formDate ?>">
												<?php echo $formDate ?>
                                            </option>
										<?php
										endwhile;
										?>
									<?php
									endif; ?>
                                </select>
                            </div>
                            <div class="form-row">
                                <label for="your-name">Naam*</label>
                                <input type="text" name="your-name" required>
                            </div>
                            <div class="form-row">
                                <label for="your-surname">Achternaam*</label>
                                <input type="text" name="your-surname" required>
                            </div>
                            <div class="form-row">
                                <label for="your-tel">Telefoon*</label>
                                <input type="text" name="your-tel" required>
                            </div>
                            <div class="form-row">
                                <label for="your-email">E-mailadres*</label>
                                <input type="email" name="your-email" required>
                            </div>
                            <div class="form-row">
                                <label for="your-address">Adres</label>
                                <input type="text" name="your-address">
                            </div>
                            <div class="form-row">
                                <label for="your-zipcode">Postcode</label>
                                <input type="text" name="your-zipcode">
                            </div>
                            <div class="form-row">
                                <label for="your-city">Woonplaats</label>
                                <input type="text" name="your-city">
                            </div>
                            <div class="form-row">
                                <label class="label-message" for="your-message">Opmerking</label>
                                <textarea name="your-message" rows="3"></textarea>
                            </div>
                            <div class="form-row">
                                <input type="submit" value="Verzenden">
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btnlog btn-secondary" data-dismiss="modal">Sluiten</button>
                    </div>
                </div>

            </div>
        </div>
	<?php endwhile; endif; ?>

	<?php
	if ( ! empty( $_POST["your-name"] ) ) :
		$post_title   = get_the_title();
		$post_date    = $_POST['date'];
		$post_name    = $_POST['your-name'];
		$post_surname = $_POST['your-surname'];
		$post_tel     = $_POST['your-tel'];
		$post_mail    = $_POST['your-email'];
		$post_address = $_POST['your-address'];
		$post_zipcode = $_POST['your-zipcode'];
		$post_city    = $_POST['your-city'];
		$post_message = $_POST['your-message'];

		$email = file_get_contents( get_template_directory_uri() . '/template-parts/email-template.html' );
		$email = str_replace( 'post_title', $post_title, $email );
		$email = str_replace( 'post_date', $post_date, $email );
		$email = str_replace( 'post_name', $post_name, $email );
		$email = str_replace( 'post_surname', $post_surname, $email );
		$email = str_replace( 'post_tel', $post_tel, $email );
		$email = str_replace( 'post_mail', $post_mail, $email );
		$email = str_replace( 'post_adres', $post_address, $email );
		$email = str_replace( 'post_zipcode', $post_zipcode, $email );
		$email = str_replace( 'post_city', $post_city, $email );
		$email = str_replace( 'post_message', $post_message, $email );

		$email_anita = file_get_contents( get_template_directory_uri() . '/template-parts/email-template-anita.html' );
		$email_anita = str_replace( 'post_title', $post_title, $email_anita );
		$email_anita = str_replace( 'post_date', $post_date, $email_anita );
		$email_anita = str_replace( 'post_name', $post_name, $email_anita );
		$email_anita = str_replace( 'post_surname', $post_surname, $email_anita );
		$email_anita = str_replace( 'post_tel', $post_tel, $email_anita );
		$email_anita = str_replace( 'post_mail', $post_mail, $email_anita );
		$email_anita = str_replace( 'post_adres', $post_address, $email_anita );
		$email_anita = str_replace( 'post_zipcode', $post_zipcode, $email_anita );
		$email_anita = str_replace( 'post_city', $post_city, $email_anita );
		$email_anita = str_replace( 'post_message', $post_message, $email_anita );

		//php mailer variables
		$to       = $post_mail;
		$to_anita = get_field( 'option_mail', 'option' );
		$from     = 'info@annagrande.nl';
		$headers  = "From: " . strip_tags( $from ) . "\r\n";
		$headers  .= "MIME-Version: 1.0\r\n";
		$headers  .= "Content-Type: text/html; charset=UTF-8\r\n";

		$sent = wp_mail( $to, 'Bedankt voor je inschrijving!', $email, $headers );
		wp_mail( $to_anita, 'Nieuwe inschrijving', $email_anita, $headers );
        $home = get_home_url();
		if ( $sent ) : ?>
            <script>
                window.location.replace("<?php echo $home; ?>/bedankt-voor-je-inschrijving");
            </script>
		<?php endif;
	endif; ?>

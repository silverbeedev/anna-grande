<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 24/01/2019
 * Time: 10:54
 */

$pagina_id             = get_the_ID();
$pagina_inleiding      = get_field( 'pagina_inleiding', $pagina_id ); ?>

<article id="single-post">
    <div class="single-content">
        <p class="page-inleiding">
		    <?php echo $pagina_inleiding; ?>
        </p>
		<?php the_content(); ?>
    </div>
</article>


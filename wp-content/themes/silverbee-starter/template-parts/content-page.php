<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Silverbee_Starter
 */

$pagina_id             = get_the_ID();
$pagina_inleiding      = get_field( 'pagina_inleiding', $pagina_id );

?>

<section class="entry-content">
    <div class="row justify-content-center">
        <div class="col-12 col-lg-12">
            <p class="page-inleiding">
				<?php echo $pagina_inleiding; ?>
            </p>
        </div>
        <div class="col-12">
			<?php the_content(); ?>
        </div>
    </div>

	<?php if ( is_page( 'individueel' ) || is_page( 25 ) || is_page( 'team' ) || is_page( 27 ) ) : ?>

		<?php
		$parent_id = get_the_ID();
		$args      = array(
			'post_parent' => $parent_id,
			'post_type'   => 'page',
			'numberposts' => - 1,
			'post_status' => 'any'
		);
		$children  = get_children( $args );
		?>

        <div class="child-page-wrapper">
            <div class="row">
				<?php foreach ( $children as $child ) :
                    $link = get_the_permalink($child); ?>
                    <div class="col-lg-4 col-12 col-sm-6 col-md-6 child-col">
                        <div class="child-page-item" onclick="location.href='<?php echo $link; ?>';">
                            <div class="item-thumb">
                                <img src="<?php echo get_the_post_thumbnail_url( $child ); ?>" alt=""/>
                                <h2>
									<?php echo $child->post_title; ?>
                                </h2>
                            </div>
                            <div class="item-excerpt">
                                <p>
									<?php echo $child->post_excerpt; ?>
                                </p>
                            </div>
                            <div class="item-cta">
                                <a href="<?php echo $link; ?>" class="btn-primary">
                                    Meer info
                                </a>
                            </div>
                        </div>
                    </div>
				<?php endforeach; ?>
            </div>
        </div>

	<?php elseif ( is_page( 'tarieven' ) || is_page( 31 ) ) : ?>
        <div class="child-page-wrapper">
            <div class="row justify-content-center">
				<?php

				if ( have_rows( 'tarieven' ) ):
					while ( have_rows( 'tarieven' ) ) : the_row();

						?>

                        <div class="col-lg-4 col-12 col-sm-6 col-md-6">
                            <a href="<?php echo get_sub_field('tarief_link'); ?>" class="nostyle">
                                <div class="child-page-item">
                                    <div class="item-thumb">
                                        <img src="<?php the_sub_field( 'tarief_afbeelding' ); ?>" alt=""/>
                                        <h2>
				                            <?php the_sub_field( 'tarief_naam' ); ?>
                                        </h2>
                                    </div>
                                    <div class="item-excerpt">
			                            <?php the_sub_field( 'tarief_informatie' ); ?>
                                    </div>
                                </div>
                            </a>
                        </div>
					<?php endwhile; ?>
                <div class="col-12 text-center">
                    <div class="item-cta tarief">
                        <a href="/contact" class="btn-primary">
                            Neem contact op
                        </a>
                    </div>
                </div>
                <?php

				endif;
				?>
            </div>
        </div>
	<?php endif; ?>
</section>




<?php
$args = array(
	'posts_per_page' => -1,
	'orderby'        => 'date',
	'order'          => 'DESC',
	'post_type'      => 'workshops',
	'post_status'    => 'publish',
);

$workshops = get_posts( $args ); ?>

<section class="entry-content">
    <div class="child-page-wrapper">
        <div class="row">
			<?php foreach ( $workshops as $workshop ) :
                $link = get_the_permalink($workshop);?>
                <div class="col-lg-4 col-md-6 child-col">
                    <div class="child-page-item" onclick="location.href='<?php echo $link; ?>';">
                        <div class="item-thumb">
                            <img src="<?php echo get_the_post_thumbnail_url( $workshop ); ?>" alt=""/>
                            <h2>
								<?php echo $workshop->post_title; ?>
                            </h2>
                        </div>
                        <div class="item-excerpt">
                            <p>
								<?php echo $workshop->post_excerpt; ?>
                            </p>
                        </div>
                        <div class="item-cta">
                            <a href="<?php echo $link; ?>" class="btn-primary">
                                Meer info
                            </a>
                        </div>
                    </div>
                </div>
			<?php endforeach; ?>
        </div>
    </div>
</section>

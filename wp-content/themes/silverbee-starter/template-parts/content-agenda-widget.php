<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 22/01/2019
 * Time: 13:15
 */

?>
<?php
$this_workshop = get_queried_object_id();

$args                        = array(
	'post_type'      => 'agenda',
	'posts_per_page' => - 1,
	'order'          => 'ASC'
);
$agendas                     = get_posts( $args );

foreach ( $agendas as $agenda ) :
	$agenda_id = $agenda->ID;
	$related_workshop        = get_field( 'agenda_item', $agenda_id );

	if ( $related_workshop === $this_workshop ) :
		$my_workshop_name = get_the_title( $agenda_id );
		$my_workshop_content = get_the_content( $agenda_id );
		$my_workshop_item    = get_field( 'agenda_item', $agenda_id );
		$title               = get_the_title();
		$the_ID              = get_the_ID();

		if ( have_rows( 'datums', $agenda_id ) ) : ?>
            <div class="agenda-archive-wrapper">
                <h3>Agenda</h3>
                <div class="agenda-item">
                    <div class="row">
                        <div class="col-3 align-self-center ">
							<?php
							while ( have_rows( 'datums', $agenda_id ) ) : the_row();
								$my_workshop_date = get_sub_field( 'datum' );
								?>
                                <div class="agenda-date">
									<?php
									$originalDate = $my_workshop_date;
									$formDate     = date( 'd-M-Y', strtotime( $originalDate ) );
									$newDate      = date_i18n( '<\s\p\a\n\>d<\/\s\p\a\n\> M', strtotime( $originalDate ) );
									echo $newDate ?>
                                </div>
							<?php
							endwhile;
							?>
                        </div>
                        <div class="col-9 align-self-center">
                            <div class="agenda-item-content">
                                <div class="item-title">
									<?php echo $my_workshop_name ?>
                                </div>

                                <div class="agenda-inschrijven">
                                    <a class="btn-primary" id="<?php echo $my_workshop_name; ?>"
                                       data-toggle="modal" data-target="#inschrijfmodal">
                                        Inschrijven
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		<?php
		endif;
	endif;
endforeach;
?>

    <div class="modal fade inschrijfmodal" id="inschrijfmodal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Inschrijven voor: <span class="modal-info"></span>
                    </h5>
                </div>
                <div class="modal-body">
                    <form action="" method="post" class="inschrijf-form" id="inschrijven-form">
                        <div class="form-row">
                            <label for="date">Datum*</label>
                            <select name="date" required>
								<?php
								foreach ( $agendas as $agenda ) :
									$agenda_id = $agenda->ID;
									$related_workshop = get_field( 'agenda_item', $agenda_id );
									if ( $related_workshop === $this_workshop ) :
										$my_workshop_name = get_the_title( $agenda_id );
										$my_workshop_item = get_field( 'agenda_item', $agenda_id );
										$title = get_the_title();
										$the_ID = get_the_ID();
										if ( have_rows( 'datums', $agenda_id ) ) :
											while ( have_rows( 'datums', $agenda_id ) ) : the_row();
												$my_workshop_date = get_sub_field( 'datum' );
												$originalDate     = $my_workshop_date;
												$formDate         = date_i18n( 'd M Y', strtotime( $originalDate ) ); ?>
                                                <option value="<?php echo $formDate; ?>">
													<?php echo $formDate; ?>
                                                </option>
											<?php endwhile;
										endif;
									endif;
								endforeach;
								?>
                            </select>
                        </div>
                        <div class="form-row">
                            <label for="your-name">Naam*</label>
                            <input type="text" name="your-name" required>
                        </div>
                        <div class="form-row">
                            <label for="your-surname">Achternaam*</label>
                            <input type="text" name="your-surname" required>
                        </div>
                        <div class="form-row">
                            <label for="your-tel">Telefoon*</label>
                            <input type="text" name="your-tel" required>
                        </div>
                        <div class="form-row">
                            <label for="your-email">E-mailadres*</label>
                            <input type="email" name="your-email" required>
                        </div>
                        <div class="form-row">
                            <label for="your-address">Adres</label>
                            <input type="text" name="your-address">
                        </div>
                        <div class="form-row">
                            <label for="your-zipcode">Postcode</label>
                            <input type="text" name="your-zipcode">
                        </div>
                        <div class="form-row">
                            <label for="your-city">Woonplaats</label>
                            <input type="text" name="your-city">
                        </div>
                        <div class="form-row">
                            <label class="label-message" for="your-message">Opmerking</label>
                            <textarea name="your-message" rows="3"></textarea>
                        </div>
                        <div class="form-row">
                            <div class="g-recaptcha" data-sitekey="6Ld1iVUmAAAAAPF6bWH1vdEwqAm2_d_YgdxrIFXe"
                                 data-callback="enableBtn" data-expired-callback="disableBtn"></div>

                            <button type="submit" class="btn btnlog btn-secondary" id="offerteMail" name="offerteMail">
                                Verzenden
                            </button>
                            <script type="text/javascript"> document.getElementById("offerteMail").disabled = true;

                              function enableBtn() {
                                document.getElementById("offerteMail").disabled = false;
                              }

                              function disableBtn() {
                                document.getElementById("offerteMail").disabled = true;
                              }
                            </script>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btnlog btn-secondary" type="button" data-dismiss="modal">Sluiten</button>
                </div>
            </div>
        </div>
    </div>

<?php
if ( isset( $_POST['g-recaptcha-response'] ) ) :
	$captcha = $_POST['g-recaptcha-response'];
	$secretKey = "6Ld1iVUmAAAAADisnwyvprM7oMtMzunBx1P6e5-U";

	$ip = $_SERVER['REMOTE_ADDR'];

	// post request to server
	$url          = 'https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode( $secretKey ) . '&response=' . urlencode( $captcha );
	$response     = file_get_contents( $url );
	$responseKeys = json_decode( $response, true );
	// should return JSON with success as true

    if ( $responseKeys["success"] ) :

	$post_title   = get_the_title();
	$post_date    = $_POST['date'];
	$post_name    = $_POST['your-name'];
	$post_surname = $_POST['your-surname'];
	$post_tel     = $_POST['your-tel'];
	$post_mail    = $_POST['your-email'];
	$post_address = $_POST['your-address'];
	$post_zipcode = $_POST['your-zipcode'];
	$post_city    = $_POST['your-city'];
	$post_message = $_POST['your-message'];

	$email = file_get_contents( get_template_directory_uri() . '/template-parts/email-template.html' );
	$email = str_replace( 'post_title', $post_title, $email );
	$email = str_replace( 'post_date', $post_date, $email );
	$email = str_replace( 'post_name', $post_name, $email );
	$email = str_replace( 'post_surname', $post_surname, $email );
	$email = str_replace( 'post_tel', $post_tel, $email );
	$email = str_replace( 'post_mail', $post_mail, $email );
	$email = str_replace( 'post_adres', $post_address, $email );
	$email = str_replace( 'post_zipcode', $post_zipcode, $email );
	$email = str_replace( 'post_city', $post_city, $email );
	$email = str_replace( 'post_message', $post_message, $email );

	$email_anita = file_get_contents( get_template_directory_uri() . '/template-parts/email-template-anita.html' );
	$email_anita = str_replace( 'post_title', $post_title, $email_anita );
	$email_anita = str_replace( 'post_date', $post_date, $email_anita );
	$email_anita = str_replace( 'post_name', $post_name, $email_anita );
	$email_anita = str_replace( 'post_surname', $post_surname, $email_anita );
	$email_anita = str_replace( 'post_tel', $post_tel, $email_anita );
	$email_anita = str_replace( 'post_mail', $post_mail, $email_anita );
	$email_anita = str_replace( 'post_adres', $post_address, $email_anita );
	$email_anita = str_replace( 'post_zipcode', $post_zipcode, $email_anita );
	$email_anita = str_replace( 'post_city', $post_city, $email_anita );
	$email_anita = str_replace( 'post_message', $post_message, $email_anita );

    //php mailer variables
	$to       = $post_mail;
	$to_anita = get_field( 'option_mail', 'option' );
	$headers = array('Content-Type: text/html; charset=UTF-8','From: Annagrande <info@annagrande.nl>');

	$sent     = wp_mail( $to, 'Bedankt voor je inschrijving!', $email, $headers );
    wp_mail( $to_anita, 'Nieuwe inschrijving', $email_anita, $headers );
	$home = get_home_url();
    if ( $sent ) : ?>
        <script>
            window.location.replace("<?php echo $home; ?>/bedankt-voor-je-inschrijving");
        </script>
	<?php endif;
    else : ?>
        <p>
           Er ging iets fout tijdens het verifieren. Probeer het nog eens.
        </p>
    <?php
    endif;
endif; ?>

<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 24/12/2018
 * Time: 15:46
 */

$args = array(
	'posts_per_page' => - 1,
	'post_type'      => 'portfolio',
	'post_status'    => 'publish',
);

$partners = get_posts( $args );

?>

<div class="slider-wrapper">
    <h2 class="fancy">
        Portfolio
    </h2>

    <div id="carousel-partners" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
			<?php
			$i = 1;

			foreach ( $partners

			as $partner ) :
			$partner_id    = $partner->ID;
			$url_not_empty = get_post_meta( $partner_id, '_portfolio_url_meta_key', true );
			?>
			<?php if ( $i == 1 ): ?>
            <div class="carousel-item <?php echo ( $i == 1 ) ? 'active' : '' ?>"><?php endif; ?>
                <div class="item-img item-<?php echo $i; ?>">
					<?php if ( ! ( $url_not_empty == null || $url_not_empty == '' ) ): ?>
                    <a target="_blank"
                       href=" <?php echo esc_url( get_post_meta( $partner_id, '_portfolio_url_meta_key', true ) ); ?>">
						<?php endif; ?>
                        <img src="<?php echo get_the_post_thumbnail_url( $partner_id ); ?>"
                             alt="<?php echo get_the_title() ?>">
						<?php if ( ! ( $url_not_empty == null || $url_not_empty == '' ) ): ?>
                    </a>
				<?php endif; ?>
                </div>
				<?php if ( $i % 3 == 0 ): ?>
            </div>
            <div class="carousel-item"><?php endif; ?>
				<?php
				$i ++;
				endforeach;
				?>
            </div>
        </div>
        <a class="carousel-control-prev" href="#carousel-partners" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carousel-partners" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>


<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Silverbee_Starter
 */

$pagina_id             = get_the_ID();
$pagina_inleiding      = get_field( 'pagina_inleiding', $pagina_id );

get_header(); ?>
    <article>
        <section id="single" class="page">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-11 col-lg-11 col-xl-7">
                        <section class="entry-content">
                            <p class="page-inleiding pb-4">
		                        <?php echo $pagina_inleiding; ?>
                            </p>
                        <?php the_content(); ?>
                        </section>
                    </div>
                </div>
                <div class="portfolio-slider-wrapper">
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <?php get_template_part('template-parts/portfolio', 'items'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </article>
<?php
get_footer();

<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Silverbee_Starter
 */

$option_adres    = get_field( 'option_adres', 'option' );
$option_postcode = get_field( 'option_postcode', 'option' );
$option_stad     = get_field( 'option_stad', 'option' );
$option_land     = get_field( 'option_land', 'option' );
$option_kvk      = get_field( 'option_kvk', 'option' );
$option_btw      = get_field( 'option_btw', 'option' );
$option_mail     = get_field( 'option_mail', 'option' );
$option_tel      = get_field( 'option_tel', 'option' );
$option_linkedin = get_field( 'option_linkedin', 'option' );
$option_facebook = get_field( 'option_facebook', 'option' );
$option_twitter  = get_field( 'option_twitter', 'option' );

?>

</div><!-- #content -->
</main>

<footer>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-11 col-lg-11 col-xl-7">
                <div class="row">
                    <div class="col-lg-4 col-sm-6 col-md-4 col-12">
                        <ul class="footer-contactgegevens">
                            <li>Anna Grande</li>
							<?php
							if ( $option_adres ) {
								echo '<li>' . $option_adres . '</li>';
							}
							if ( $option_postcode ) {
								echo '<li>' . $option_postcode . ' ' . $option_stad . '</li>';
							}
							if ( $option_land ) {
								echo '<li>' . $option_land . '</li>';
							}
							if ( $option_kvk ) {
								echo '<li>KVK-Nummer: ' . $option_kvk . '</li>';
							}
							if ( $option_btw ) {
								echo '<li>BTW-Nummer: ' . $option_btw . '</li>';
							}
							?>
                        </ul>
                    </div>
                    <div class="col-lg-4 col-sm-6 col-md-4 col-12">
                        <nav id="site-navigation" class="main-navigation">
							<?php
							wp_nav_menu( array(
								'theme_location' => 'menu-1',
								'menu_id'        => 'footer-menu',
							) );
							?>
                        </nav><!-- #site-navigation -->
                    </div>
                    <div class="col-lg-4 col-sm-6 col-md-4 col-12">
                            <span class="footer-contact-title">
                                Contact
                            </span>
						<?php echo do_shortcode( '[contact-form-7 id="43" title="Footer contactformulier"]' ); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="footer-mail-wrapper">
							<?php
							if ( $option_mail ) {
								include( get_template_directory() . '/img/icons/mail.svg' );
								echo '<a href="mailto:' . $option_mail . '">' . $option_mail . '</a>';
							}
							?>
                            <span class="copyright"><?php echo date( "Y" ); ?> &copy; Copyright Anna Grande</span>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="footer-phone-wrapper">
							<?php if ( $option_tel ) {
								include( get_template_directory() . '/img/icons/phone.svg' );
								echo '<a href="tel:' . $option_tel . '">' . $option_tel . '</a>';
							}
							?>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="footer-social-wrapper">
							<?php
							if ( $option_linkedin ) {
								echo '<a href="' . $option_linkedin . '" target="blank">';
								include( get_template_directory() . '/img/icons/linkedin.svg' );
								echo '</a>';
							}
							if ( $option_facebook ) {
								echo '<a href="' . $option_facebook . '" target="blank">';
								include( get_template_directory() . '/img/icons/facebook.svg' );
								echo '</a>';
							}
							if ( $option_twitter ) {
								echo '<a href="' . $option_twitter . '" target="blank">';
								include( get_template_directory() . '/img/icons/twitter.svg' );
								echo '</a>';
							}
							?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>

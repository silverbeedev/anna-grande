<?php
/**
 * The template for displaying the front-page
 *
 * @package Silverbee_Starter
 */

get_header(); ?>
    <article>
        <div id="front-page" class="page">
            <section id="section-1">
                <div class="container-fluid">
                    <div class="row justify-content-center">
                        <div class="col-11 col-xl-10">
                            <div class="page-content">
                                <?php get_template_part('template-parts/front-page/front', 'cpt'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="section-2">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="page-content">
	                            <?php get_template_part('template-parts/front-page/front', 'ervaringen'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="section-3">
                <div class="container-fluid">
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <div class="page-content">
						        <?php get_template_part('template-parts/front-page/front', 'partners'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </article>
<?php
get_footer();

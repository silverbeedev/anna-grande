(function ($) {
    $(document).ready(function(){

        $('#carousel-ervaringen').carousel({
            interval: 8000
        });

        $('#carousel-partners').carousel({
            interval: 8000
        });

        $( ".navbar-toggle" ).toggle(function() {
            $(".rotatemenu").addClass("isactive");
            $(".overlay").addClass("isactive");
            $("html").css("overflow-y","hidden");
        }, function() {
            $(".rotatemenu").removeClass("isactive");
            $(".overlay").removeClass("isactive");
            $("html").css("overflow-y", "auto");
        });


        $('.agenda-inschrijven > .btn-primary').click(function(){
            var item_value = $(this).attr('id');
            console.log(item_value);

            $('input[name="agenda-item"]').val(item_value);
            $('.inschrijfmodal .modal-info').html(item_value);
        });
    });
})(jQuery);

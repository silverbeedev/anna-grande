<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Silverbee_Starter
 */

get_header(); ?>
    <article>
        <section id="single" class="page">
            <div class="container-fluid">
                <div class="header-page-wrapper">
                    <div class="row justify-content-center">
                        <div class="col-11 col-xl-7">
                            <h1 class="fancy">
					            <?php echo get_the_title(); ?>
                            </h1>
	                        <?php if ( get_field('pagina_ondertitel') ) : ?>
                                <span class="page-ondertitel">
                                    <?php echo get_field('pagina_ondertitel'); ?>
                                </span>
	                        <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-6 col-12 col-sm-6">
						<?php
						if ( is_singular( 'workshops' ) ) {
							get_template_part( 'template-parts/content', 'single' );
						} else {
							get_template_part( 'template-parts/content', get_post_format() );
						}
						?>
                    </div>
                    <div class="col-lg-4 col-xl-3 offset-xl-1 col-12 col-sm-6 ">
						<?php
						if ( is_singular( 'workshops' ) ) {
							get_template_part( 'template-parts/content', 'agenda-widget' );
						}
						?>
                    </div>
                </div>
            </div>
        </section>
    </article>
<?php
get_footer();

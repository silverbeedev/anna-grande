<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Silverbee_Starter
 */

?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <!--    get analytics script-->
	<?php get_template_part( 'template-parts/analytics/content', 'head' ); ?>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <script src="https://www.google.com/recaptcha/api.js"></script>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<!--    get analytics script-->
<?php get_template_part( 'template-parts/analytics/content', 'body' ); ?>
<div id="page" class="site">
    <header id="masthead" class="site-header <?php if ( ! is_front_page() ) : ?>page-header<?php endif; ?>"
            role="banner">
		<?php
            $page_header_bg = get_the_post_thumbnail_url();
            $page_no_header_bg = get_header_image();
            if ( is_front_page() ) :
                $page_header_bg = get_template_directory_uri().'/img/backgrounds/header-bg.png';
            elseif ( is_archive() && !is_post_type_archive('workshops') ) :
	            $page_header_bg = get_header_image();
            elseif ( is_post_type_archive('workshops') ) :
	            $page_header_bg = get_the_post_thumbnail_url(29);
            endif;
        ?>
        <div id="primary-header" <?php if( !is_front_page() || !$page_header_bg ) : ?>style="background-image: url('<?php echo $page_header_bg; ?>')"<?php endif; ?>>

            <?php if ( is_front_page() ) :
            $front_header_bg = get_field('header_video');
            ?>
            <div class="video-background">
                    <div class="video-inner">
                        <video autoplay muted loop>
                            <source src="/wp-content/themes/silverbee-starter/img/video/agrande.webm" type="video/webm">
                            <source src="/wp-content/themes/silverbee-starter/img/video/agrande.mp4" type="video/mp4">
                            Your browser does not support the HTML5 Video element.
                        </video>
                    </div>
            </div>
            <?php endif; ?>

            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-12">
                        <div class="header-navigation-wrapper">
                            <div class="container-fluid">
                                <div class="row justify-content-center">
                                    <div class="col-11 col-xl-10">
                                        <div class="row">
                                            <div class="col-lg-2 col-xl-2 col-md-4 col-sm-6 col-12">
                                                <div class="site-brand">
													<?php the_custom_logo(); ?>
                                                </div>
                                            </div>
                                            <div class="col-lg-10 col-xl-10 col-md-8 col-sm-6 col-12 align-self-center text-right">
                                                <div class="site-menu-contact">
                                                    <span>
                                                        <?php
                                                        $option_mail     = get_field( 'option_mail', 'option' );
                                                        $option_tel      = get_field( 'option_tel', 'option' ); ?>

                                                        <?php
                                                        if ( $option_tel ) {
	                                                        include( get_template_directory() . '/img/icons/phone.svg' );
	                                                        echo '<a href="tel:' . $option_tel . '">' . $option_tel . '</a>';
                                                        }
                                                        ?>
                                                    </span>
                                                    <span>
                                                        <?php
                                                        if ( $option_mail ) {
	                                                        include( get_template_directory() . '/img/icons/mail.svg' );
	                                                        echo '<a href="mailto:' . $option_mail . '">' . $option_mail . '</a>';
                                                        }
                                                        ?>
                                                    </span>
                                                </div>
                                                <div class="site-menu">
                                                    <nav id="site-navigation" class="main-navigation headermenu">
														<?php
														wp_nav_menu( array(
															'theme_location' => 'menu-1',
															'menu_id'        => 'primary-menu',
														) );
														?>
                                                    </nav><!-- #site-navigation -->
                                                    <nav class="navbar hamburger hidden navbar-default">
                                                            <div class="navbar-header">
                                                                <button type="button" class="navbar-toggle"  data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                                                    <span class="sr-only">Toggle navigation</span>
                                                                    <div class="rotatemenu">
                                                                        <span class="icon-bar"></span>
                                                                        <span class="icon-bar"></span>
                                                                        <span class="icon-bar"></span>
                                                                    </div>
                                                                </button>
                                                            </div>

                                                            <!-- Collect the nav links, forms, and other content for toggling -->
                                                            <div class="overlay" id="bs-example-navbar-collapse-1">
                                                                <div class="row ">
                                                                    <div class="col-lg-8 col-12">
                                                                        <div class="logorow text-center">
                                                                           <?php the_custom_logo(); ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row justify-content-center">
                                                                    <div class="col-8">
						                                                <?php wp_nav_menu( array(
							                                                'theme_location' => 'menu-1',
							                                                'menu'        => 'primary'
						                                                ) ); ?>
                                                                    </div>
                                                                </div>
                                                            </div><!-- /.navbar-collapse -->
                                                    </nav>


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						<?php if ( is_front_page() ) : ?>
                            <div class="header-content-wrapper">
                                <div class="row justify-content-center">
                                    <div class="col-11 col-lg-10">
                                        <div class="row justify-content-end">
                                            <div class="col-lg-6 col-12">
												<?php the_content(); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
						<?php else : ?>

						<?php endif; ?>
                    </div>
                </div>
            </div>
			<?php if ( is_front_page() ) : ?>
                <div class="featured-workshop-wrapper">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12 col-lg-12 col-xl-6">
								<?php get_template_part( 'template-parts/front-page/front-header', 'workshop' ) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="chevron-wrapper">
					<?php include( get_template_directory() . '/img/icons/chevron.svg' ); ?>
                </div>
			<?php endif; ?>
        </div>
    </header><!-- #masthead -->

    <main>
        <div id="content" class="site-content">

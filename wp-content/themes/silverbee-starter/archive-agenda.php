<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Silverbee_Starter
 */
get_header(); ?>
    <article>
        <section id="agenda" class="page">
            <div class="container-fluid">
                <div class="header-page-wrapper">
                    <div class="row justify-content-center">
                        <div class="col-11 col-xl-7">
                            <h1 class="fancy">
                                Agenda
                            </h1>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-xl-6 col-lg-11 col-md-12">
						<?php
						get_template_part( 'template-parts/content', 'agenda' );
						?>
                    </div>
                </div>
            </div>
        </section>
    </article>
<?php
get_footer();

<?php

function create_partners() {

	$e_args = array(
		'labels'        => array(
			'name'               => __( 'Partners', 'annagrande' ),
			'singular'           => __( 'Partner', 'annagrande' ),
			'new_item'           => __( 'Nieuwe ervarin', 'annagrande' ),
			'add_new_item'       => __( 'Voeg nieuwe partner toe', 'annagrande' ),
			'add_new'           => __( 'Voeg nieuwe partner toe', 'annagrande' ),
			'edit_item'          => __( 'Bewerk partner', 'annagrande' ),
			'view_item'          => __( 'Bekijk partner', 'annagrande' ),
			'all_items'          => __( 'Alle partners items', 'annagrande' ),
			'search_items'       => __( 'Zoek partners items', 'annagrande' ),
			'not_found'          => __( 'Geen partners items gevonden.', 'annagrande' ),
			'not_found_in_trash' => __( 'Geen partners items gevonden in de prullenbak.', 'annagrande' )
		),
		'public'        => true,
		'has_archive'   => true,
		'show_ui'       => true,
		'show_in_nav_menus' => true,
		'supports'      => array( 'title', 'editor', 'excerpt', 'thumbnail'),
		'menu_position' => 5,
		'hierarchical'  => false,
		'menu_icon'     => 'dashicons-groups'
	);

//Registers CPTs
	register_post_type( 'partners', $e_args );

}



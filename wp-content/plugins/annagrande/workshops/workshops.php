<?php
function create_workshops() {
	$e_args = array(
		'labels'        => array(
			'name'               => __( 'Workshops', 'annagrande' ),
			'singular'           => __( 'Workshop', 'annagrande' ),
			'add_new_item'       => __( 'Voeg nieuwe workshops toe', 'annagrande' ),
			'new_item'           => __( 'Nieuwe workshop', 'annagrande' ),
			'edit_item'          => __( 'Bewerk workshop', 'annagrande' ),
			'view_item'          => __( 'Bekijk workshop', 'annagrande' ),
			'all_items'          => __( 'Alle workshops', 'annagrande' ),
			'search_items'       => __( 'Zoek workshops', 'annagrande' ),
			'not_found'          => __( 'Geen workshops gevonden.', 'annagrande' ),
			'not_found_in_trash' => __( 'Geen workshops gevonden in de prullenbak.', 'annagrande' )
		),
        'public' => true,
        'publicly_queryable' => true,
        'has_archive'   => true,
		'show_ui'       => true,
		'supports'      => array( 'title', 'editor', 'excerpt', 'custom-fields', 'thumbnail', 'page-attributes' ),
		'hierarchical'  => false, 
		'menu_position' => 5,
		'menu_icon'     => 'dashicons-feedback'
	);
    //Registers CPTs
	register_post_type( 'workshops', $e_args );
	require_once( dirname( __FILE__ ) . '/../acf/workshops.php' );

}
<?php

function create_portfolio() {

    $e_args = array(
        'labels'        => array(
            'name'               => __( 'Portfolio', 'annagrande' ),
            'singular'           => __( 'Portfolio', 'annagrande' ),
            'new_item'           => __( 'Nieuwe portfolio item', 'annagrande' ),
            'add_new_item'       => __( 'Voeg nieuwe portfolio item toe', 'annagrande' ),
            'add_new'           => __( 'Voeg nieuwe portfolio item toe', 'annagrande' ),
            'edit_item'          => __( 'Bewerk portfolio', 'annagrande' ),
            'view_item'          => __( 'Bekijk portfolio', 'annagrande' ),
            'all_items'          => __( 'Alle portfolio items', 'annagrande' ),
            'search_items'       => __( 'Zoek portfolio items', 'annagrande' ),
            'not_found'          => __( 'Geen portfolio items gevonden.', 'annagrande' ),
            'not_found_in_trash' => __( 'Geen portfolio items gevonden in de prullenbak.', 'annagrande' )
        ),
        'public'        => true,
        'has_archive'   => true,
        'show_ui'       => true,
        'show_in_nav_menus' => true,
        'supports'      => array( 'title', 'editor', 'excerpt', 'thumbnail'),
        'menu_position' => 5,
        'hierarchical'  => false,
        'menu_icon'     => 'dashicons-groups'
    );

//Registers CPTs
    register_post_type( 'portfolio', $e_args );

}



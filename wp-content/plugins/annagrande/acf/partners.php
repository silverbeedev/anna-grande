<?php
/**
 * Created by PhpStorm.
 * User: tomaszwilczek
 * Date: 26/02/2019
 * Time: 15:08
 */


abstract class PartnerMetaBox
{
    public static function add()
    {
        $screens = ['post', 'partners'];
        foreach ($screens as $screen) {
            add_meta_box(
                'partner_url_id',          // Unique ID
                'Partner url', // Box title
                [self::class, 'html'],   // Content callback, must be of type callable
                $screen,                  // Post type
                'side'
            );
        }
    }

    public static function save($post_id)
    {

        update_post_meta(
            $post_id,
            '_partner_url_meta_key',
            $_POST['partner_url_field']
        );

    }

    public static function html($post)
    {
        $value = get_post_meta($post->ID, '_partner_url_meta_key', true);
        ?>

        <p>
            <label for="partner_url_field">Partner item URL</label>
            <input type="url" name="partner_url_field" value="<?php echo $value ?>"/>

        </p>

        <?php
    }
}


add_action('add_meta_boxes', ['PartnerMetaBox', 'add']);
add_action('save_post', ['PartnerMetaBox', 'save']);
<?php
/**
 * Created by PhpStorm.
 * User: tomaszwilczek
 * Date: 26/02/2019
 * Time: 15:08
 */


abstract class PortfolioMetaBox
{
    public static function add()
    {
        $screens = ['post', 'portfolio'];
        foreach ($screens as $screen) {
            add_meta_box(
                'portfolio_url_id',          // Unique ID
                'Portfolio url', // Box title
                [self::class, 'html'],   // Content callback, must be of type callable
                $screen,                  // Post type
                'side'
            );
        }
    }

    public static function save($post_id)
    {

        update_post_meta(
            $post_id,
            '_portfolio_url_meta_key',
            $_POST['portfolio_url_field']
        );

    }

    public static function html($post)
    {
        $value = get_post_meta($post->ID, '_portfolio_url_meta_key', true);
        ?>

        <p>
            <label for="portfolio_url_field">Portfolio item URL</label>
            <input type="url" name="portfolio_url_field" value="<?php echo $value ?>"/>

        </p>

        <?php
    }
}


add_action('add_meta_boxes', ['PortfolioMetaBox', 'add']);
add_action('save_post', ['PortfolioMetaBox', 'save']);
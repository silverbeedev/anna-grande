<?php
abstract class UitgelichtMetaBox
{
    public static function add()
    {
        $screens = ['post', 'workshops', 'page'];
        foreach ($screens as $screen) {
            add_meta_box(
                'uitgelicht_id',          // Unique ID
                'Uitgelicht', // Box title
                [self::class, 'html'],   // Content callback, must be of type callable
                $screen,                  // Post type
                'side'
            );
        }
    }

    public static function save($post_id)
    {

            update_post_meta(
                $post_id,
                '_uitgelicht_meta_key',
                $_POST['uitgelicht_field']
            );

    }

    public static function html($post)
    {
        $value = get_post_meta($post->ID, '_uitgelicht_meta_key', true);
        ?>

        <p>
        <input type="checkbox" name="uitgelicht_field" value="checkbox" <?php if ( $value === 'checkbox' ) echo 'checked'; ?>/>
        <label for="uitgelicht_field">Post uitlichten</label>
        </p>

        <?php
    }
}

add_action('add_meta_boxes', ['UitgelichtMetaBox', 'add']);
add_action('save_post', ['UitgelichtMetaBox', 'save']);
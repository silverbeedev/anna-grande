<?php
/**
 * The plugin bootstrap file
 *
 *
 * @link              http://silverbee.nl
 * @since             1.0.0
 * @package           annagrande
 *
 * @wordpress-plugin
 * Plugin Name:       Anna Grande core
 * Plugin URI:        http://silverbee.nl/
 * Description:       Anna Grande Custom Plugin.
 * Version:           1.0.0
 * Author:            Silverbee
 * Author URI:        http://silverbee.nl/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       ltc
 * Domain Path:       /languages
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

require_once(dirname(__FILE__) . '/agenda/agenda.php');
require_once(dirname(__FILE__) . '/workshops/workshops.php');
require_once(dirname(__FILE__) . '/ervaringen/ervaringen.php');
require_once(dirname(__FILE__) . '/portfolio/portfolio.php');
require_once(dirname(__FILE__) . '/partners/partners.php');
require_once( dirname( __FILE__ ) . '/acf/agenda.php' );
require_once( dirname( __FILE__ ) . '/acf/partners.php' );

require_once( dirname( __FILE__ ) . '/acf/portfolio.php' );
require_once( dirname( __FILE__ ) . '/acf/workshops.php' );
require_once( dirname( __FILE__ ) . '/acf/divers.php' );

function annagrande_add_custom_post_types() {
    create_agenda();
	create_workshops();
	create_ervaringen();
	create_partners();
    create_portfolio();
}

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page('Bedrijfsgegevens');
}

add_action( 'init', 'annagrande_add_custom_post_types' );

// Replace Posts label as Articles in Admin Panel



<?php
/**
 * Created by PhpStorm.
 * User: development
 * Date: 17/10/2017
 * Time: 15:03
 */
function create_agenda() {

	$e_args = array(
		'labels'        => array(
			'name'               => __( 'Agenda', 'annagrande' ),
			'singular'           => __( 'Agenda', 'annagrande' ),
            'new_item'           => __( 'Nieuwe Agenda item', 'annagrande' ),
			'add_new_item'       => __( 'Voeg nieuwe Agenda item toe', 'annagrande' ),
            'add_new'           => __( 'Voeg nieuwe Agenda item toe', 'annagrande' ),
			'edit_item'          => __( 'Bewerk Agenda item', 'annagrande' ),
			'view_item'          => __( 'Bekijk Agenda item', 'annagrande' ),
			'all_items'          => __( 'Alle Agenda items', 'annagrande' ),
			'search_items'       => __( 'Zoek Agenda items', 'annagrande' ),
			'not_found'          => __( 'Geen Agenda items gevonden.', 'annagrande' ),
			'not_found_in_trash' => __( 'Geen Agenda items gevonden in de prullenbak.', 'annagrande' )
		),
		'public'        => true,
		'has_archive'   => true,
		'show_ui'       => true,
		'show_in_nav_menus' => true,
		'supports'      => array( 'title', 'editor', 'excerpt', 'thumbnail'),
		'menu_position' => 5,
        'hierarchical'  => false,
		'menu_icon'     => 'dashicons-calendar-alt'
	);

//Registers CPTs
	register_post_type( 'agenda', $e_args );


	require_once( dirname( __FILE__ ) . '/../acf/agenda.php' );
}



<?php

function create_ervaringen() {

	$e_args = array(
		'labels'        => array(
			'name'               => __( 'Ervaringen', 'annagrande' ),
			'singular'           => __( 'ervaring', 'annagrande' ),
			'new_item'           => __( 'Nieuwe ervarin', 'annagrande' ),
			'add_new_item'       => __( 'Voeg nieuwe ervaring toe', 'annagrande' ),
			'add_new'           => __( 'Voeg nieuwe ervaring toe', 'annagrande' ),
			'edit_item'          => __( 'Bewerk ervaring', 'annagrande' ),
			'view_item'          => __( 'Bekijk ervaring', 'annagrande' ),
			'all_items'          => __( 'Alle ervaringen items', 'annagrande' ),
			'search_items'       => __( 'Zoek ervaringen items', 'annagrande' ),
			'not_found'          => __( 'Geen ervaringen items gevonden.', 'annagrande' ),
			'not_found_in_trash' => __( 'Geen ervaringen items gevonden in de prullenbak.', 'annagrande' )
		),
		'public'        => true,
		'has_archive'   => true,
		'show_ui'       => true,
		'show_in_nav_menus' => true,
		'supports'      => array( 'title', 'editor', 'excerpt', 'thumbnail'),
		'menu_position' => 5,
		'hierarchical'  => false,
		'menu_icon'     => 'dashicons-format-status'
	);

//Registers CPTs
	register_post_type( 'ervaringen', $e_args );


	require_once( dirname( __FILE__ ) . '/../acf/ervaringen.php' );
}


